using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using System;
using System.Threading;
using System.Reflection;

public class RefillTarget : MonoBehaviour
{

    //VARIABLES
    
    //Refill Points
    public List<GameObject> targetObjects;

    public List<GameObject> hungerPoints;
    public List<GameObject> energyPoints;
    public List<GameObject> hygienePoints;
    public List<GameObject> entertainmentPoints;

    //private GameObject currentTarget;
    private GameObject hungerTarget;
    private GameObject energyTarget;
    private GameObject hygieneTarget;
    private GameObject entertainmentTarget;

    public bool hungerStatus;
    private bool energyStatus;
    private bool hygieneStatus;
    private bool enterainmentStatus;

    public Slider hungerSlider;
    public Slider energySlider;
    public Slider hygieneSlider;
    public Slider entertainmentSlider;

    private NavMeshAgent agent;
    private Animator animator;

    public float idleDistance = 0.6f;
    //private int targetCounter = 0;
    private int hungerCounter = 0;
    private int energyCounter = 0;
    private int hygieneCounter = 0;
    private int entertainmentCounter = 0;
       //private bool isFull = false;
    // Start is called before the first frame update
    
    void startingStats()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        //currentTarget = targetObjects[targetCounter];

        //Targets Initiation
        hungerTarget = hungerPoints[hungerCounter];
        energyTarget = energyPoints[energyCounter];
        hygieneTarget = hygienePoints[hygieneCounter];
        entertainmentTarget = entertainmentPoints[entertainmentCounter];
        
        //Sliders Initiation
        hungerSlider.value = 100f;
        energySlider.value = 100f;
        hygieneSlider.value = 100f;
        entertainmentSlider.value = 100f;

        //Status Initiation

    }
    void Start()
    {
        hungerStatus = true;
        energyStatus = true;
        hygieneStatus = true;
        enterainmentStatus = true;
        startingStats();
    }

    private double deFiller(float speed) //Decrasing Sliders Value
    {
            float value = 100.0f - (Time.time * speed % 100.0f);
            return value;
    }

    private double Filler(float speed) //Increasing Sliders Value
    {
            float value = Time.time * speed % 100.0f;
            return value;
    }

    void TargetPicker(List<GameObject> targetList ,GameObject currentTarget, int targetCounter)
    {
        agent.SetDestination(currentTarget.transform.position);
        if (Vector3.Distance(transform.position, currentTarget.transform.position) > idleDistance)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
            targetCounter++;
            if (targetCounter >= targetList.Count)
            {
                targetCounter = 0;
            }
            currentTarget = targetList[targetCounter];
        }
    }

    void Refiller(Slider Stat, bool isFull, float deFillSpeed)
    {
        Debug.Log("Stat.Value = " + Stat.value);
        Debug.Log("isFull = " + isFull);
        if (Stat.value >= 0 && isFull == true)
        {
                Stat.value = Convert.ToSingle(deFiller(deFillSpeed));
        }
        else
        {
            hungerStatus = false;
        }
        
        if(Stat.value <= 100 && isFull == false)
        {
                Stat.value = Convert.ToSingle(Filler(10.0f));
        }
        else
        {
            hungerStatus = true;
        }

    }

    void Update()
    {
        if (hungerSlider.value >= 0 && hungerStatus == true)
        {
            if (hungerSlider.value <= 2)
            {
                hungerStatus = false;
            }
            else
            {
                hungerSlider.value = Convert.ToSingle(deFiller(10f));
            }
        }
        else
        {
            if(hungerSlider.value >= 98)
            {
                hungerStatus = true;
            }
            else
            {
                hungerSlider.value = Convert.ToSingle(Filler(10f));
            }
        }
       // Refiller(hungerSlider, hungerStatus, 5.0f);
        //Refiller(energySlider, energyStatus, 3.0f);
        //Refiller(hygieneSlider, hygieneStatus, 4.0f);
       // Refiller(entertainmentSlider, enterainmentStatus, 2.0f);
    }
}
